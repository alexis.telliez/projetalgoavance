package DevilliersTelliez;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {

    public static void main (String [] args){

        Data data1 = new Data("src/instances/instance01.dat");
        Data data2 = new Data("src/instances/instance02.dat");
        Data data3 = new Data("src/instances/instance03.dat");
        Data data4 = new Data("src/instances/instance04.dat");
        Data data5 = new Data("src/instances/instance05.dat");
        Data dataTest = new Data("src/instances/instanceTest.dat");

        Scanner input =   new java.util.Scanner(System.in);
        boolean inputTrue;
        String option = "default";
        do {
            inputTrue = true;
            System.out.println("Option algo:" + "\n"+
                    "AireMax      -> A" + "\n"+
                    "Encombrement -> E" + "\n"+
                    "Random       -> R" + "\n"+
                    "Default      -> D");
            String opt = input.next();

            switch (opt) {
                case "A" -> option = "AireMax";
                case "E" -> option = "Encombrement";
                case "R" -> option = "Random";
                case "D" -> option = "Default";
                default -> {
                    System.out.println("Ceci n'est pas une option valide");
                    inputTrue = false;
                }
            }
        } while(!inputTrue);

        int optionHDV;
        do{
            System.out.println("Option hdv:" + "\n" +
                    "Hdv haut gauche -> 1" + "\n" +
                    "Hdv Centre      -> 2" );
            optionHDV = input.nextInt();
            switch(optionHDV){
                case 1, 2 -> inputTrue = true;
                default-> inputTrue = false;
            }
        } while(!inputTrue);


        Ecriture ec = new Ecriture("src/data/"+ option + optionHDV +".dat");


        ec.EcrireLigne(option + "\n");

        System.out.println("Data 1 : ");
        data1.setOption(optionHDV);
        data1.greedyMethod(option);
        int nbBuildPlaced1 = data1.getNbBuildPlaced();
        if(option.equals("Random")){
            List<Integer> listBuild = data1.getNbPlaced();
            List<Integer> listCase = data1.getNbCasesPlaced();
            List<Double> list = average(listBuild,listCase );
            ec.EcrireLigne("Instance 01 : Moyenne : Cases " + list.get(0) + " Bâtiments "+ list.get(1));
            ec.EcrireLigne("Instance 01 : Écart Type : Cases " + list.get(4) + " Bâtiments "+ list.get(5));
        } else
            ec.EcrireLigne("Instance 01: " + data1.evalField() + " cases occupées avec " + nbBuildPlaced1 + " placés ");
        data1.printField();

        System.out.println("Data 2 : ");
        data2.setOption(optionHDV);
        data2.greedyMethod(option);
        int nbBuildPlaced2 = data2.getNbBuildPlaced();

        if(option.equals("Random")){
            List<Integer> listBuild = data2.getNbPlaced();
            List<Integer> listCase = data2.getNbCasesPlaced();
            List<Double> list = average(listBuild,listCase );
            ec.EcrireLigne("Instance 02 : Moyenne : Cases " + list.get(0) + " Bâtiments "+ list.get(1));
            ec.EcrireLigne("Instance 02 : Écart Type : Cases " + list.get(4) + " Bâtiments "+ list.get(5));
        } else
            ec.EcrireLigne("Instance 02: " + data2.evalField() + " cases occupées avec " + nbBuildPlaced2 + " placés ");
        data2.printField();


        System.out.println("Data 3 : ");
        data3.setOption(optionHDV);
        data3.greedyMethod(option);
        int nbBuildPlaced3 = data3.getNbBuildPlaced();

        if(option.equals("Random")){
            List<Integer> listBuild = data3.getNbPlaced();
            List<Integer> listCase = data3.getNbCasesPlaced();
            List<Double> list = average(listBuild,listCase );
            ec.EcrireLigne("Instance 03 : Moyenne : Cases " + list.get(0) + " Bâtiments "+ list.get(1));
            ec.EcrireLigne("Instance 03 : Écart Type : Cases " + list.get(4) + " Bâtiments "+ list.get(5));
        } else
            ec.EcrireLigne("Instance 03: " + data3.evalField() + " cases occupées avec " + nbBuildPlaced3 + " placés ");
        data3.printField();

        System.out.println("Data 4 : ");
        data4.setOption(optionHDV);
        data4.greedyMethod(option);
        int nbBuildPlaced4 = data4.getNbBuildPlaced();
        if(option.equals("Random")){
            List<Integer> listBuild = data4.getNbPlaced();
            List<Integer> listCase = data4.getNbCasesPlaced();
            List<Double> list = average(listBuild,listCase );
            ec.EcrireLigne("Instance 04 : Moyenne : Cases " + list.get(0) + " Bâtiments "+ list.get(1));
            ec.EcrireLigne("Instance 04 : Écart Type : Cases " + list.get(4) + " Bâtiments "+ list.get(5));
        } else
            ec.EcrireLigne("Instance 04: " + data4.evalField() + " cases occupées avec " +  nbBuildPlaced4 + " placés ");

        data4.printField();

        System.out.println("Data 5 : ");
        data5.setOption(optionHDV);
        data5.greedyMethod(option);
        int nbBuildPlaced5 = data5.getNbBuildPlaced();
        if(option.equals("Random")){
            List<Integer> listBuild = data5.getNbPlaced();
            List<Integer> listCase = data5.getNbCasesPlaced();
            List<Double> list = average(listBuild,listCase );
            ec.EcrireLigne("Instance 05 : Moyenne : Cases " + list.get(0) + " Bâtiments "+ list.get(1));
            ec.EcrireLigne("Instance 05 : Écart Type : Cases " + list.get(4) + " Bâtiments "+ list.get(5));
        } else
            ec.EcrireLigne("Instance 05: " + data5.evalField() + " cases occupées avec " + nbBuildPlaced5 + " placés ");
        data5.printField();

        /*System.out.println("Data Test : ");
        dataTest.setOption(optionHDV);
        dataTest.greedyMethod(option);
        nbBuildPlaced = data1.getNbBuildPlaced();
        ec.EcrireLigne("Data Test -> Nb case -> " + dataTest.evalField());
        dataTest.printField();*/
        ec.FermetureFichier();
    }

    public static List<Double> average (List<Integer> b, List<Integer> c) {
        List<Double> toReturn = new ArrayList<>(2);
        int max = b.size();
        double cmpB = 0;
        double cmpC = 0;
        for (Integer integer : b) {
            cmpB += integer;
        }
        for (Integer integer : c) {
            cmpC += integer;
        }
        double moyenneC = (double) Math.round((cmpC/max) * 100) / 100;
        double moyenneB = (double) Math.round((cmpB/max) * 100) / 100;

        double varianceC = 0;
        double varianceB = 0;

        for (Integer integer : b) {
            varianceB += (integer - moyenneB)*(integer - moyenneB);
        }
        for (Integer integer : c) {
            varianceC += (integer - moyenneC)*(integer - moyenneC);
        }
        varianceB = (double) Math.round((varianceB/max) * 100) / 100;
        varianceC = (double) Math.round((varianceC/max) * 100) / 100;
        double ecartB = (double) Math.round(Math.sqrt(varianceB) * 100) / 100;
        double ecartC = (double) Math.round(Math.sqrt(varianceC) * 100) / 100;
        toReturn.add(moyenneC);
        toReturn.add(moyenneB);
        toReturn.add(varianceC);
        toReturn.add(varianceB);
        toReturn.add(ecartC);
        toReturn.add(ecartB);

        return toReturn;
    }
}
