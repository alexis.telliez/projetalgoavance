package DevilliersTelliez;

public class Building {
    private int x;
    private int y;
    private final int value;
    private String color;

    public Building(int w, int h, int value) {
        this.x = w;
        this.y = h;
        this.value = value;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getValue() {
        return value;
    }

    public String getColor () {
        return color;
    }

    public void setColor (String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Building : " + "\n" +
                "width = " + x + "\n" +
                "height = " + y + "\n" +
                "Value = " + value;
    }
}
