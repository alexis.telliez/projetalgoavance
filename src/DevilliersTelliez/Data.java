package DevilliersTelliez;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Data {


    private final static int DEFAULT_VALUE = 0;
    private final static int ROAD_VALUE = -1;
    private final File file; // Lecture du fichier
    private Scanner sc; // Lecture du fichier
    private int [][] field; // Taille du terrain
    private int nbBuild; // Nombre de batiments
    private int sizeGridX; // Largeur de la grille
    private int sizeGridY; // Hauteur de la grille
    private int option = 1;
    // Option 1 hdv haut gauche
    // Option 2 hdv centre
    // Par defaut 1


    private int nbBuildPlaced;
    private List<Integer> nbPlaced = new ArrayList<>();
    private List<Integer> nbCasesPlaced = new ArrayList<>();

    private List<Building> listBuild;
    private Map<Building, List<Coords>> mapCoords;
    private Coords hdvCoords = new Coords(0,0);
    private Colors color ;

    public Data(String name) {
        this.file = new File(name);
        loadFile();

    }

    public void loadFile(){
        mapCoords = new HashMap<>();
        nbBuildPlaced = 0;
        try {
            this.sc = new Scanner(this.file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        sizeGridX = sc.nextInt();
        sizeGridY = sc.nextInt();
        field = new int[sizeGridX][sizeGridY];
        initField();
        nbBuild = sc.nextInt();
        listBuild = new ArrayList<>(nbBuild);
        for(int i = 0; i < nbBuild ; i++) {
            Building bg = new Building(sc.nextInt(), sc.nextInt(), i+1);
            listBuild.add(bg);
        }
        sc.close();
        initColors();
    }
    public void setOption (int option) {
        this.option = option;

    }

    public void initColors(){
        color = new Colors();
        List<String> colors = color.getColors();
        Random rd = new Random();
        listBuild.get(0).setColor(colors.get(0));
        colors.remove(0);
        for(int b = 1 ; b < nbBuild ; b++) {
            int i = rd.nextInt(colors.size());
            listBuild.get(b).setColor(colors.get(i));
            colors.remove(i);
        }
    }

    // Initialisation du terrain avec la valeur par defaut 0
    public void initField(){
        for(int i = 0 ; i < sizeGridX; i++) {
            for (int j = 0; j < sizeGridY; j++) {
                field[i][j] = DEFAULT_VALUE;
            }
        }

    }

    //Place les batiments sur le terrain
    public void placeBuild(int index){
        List<Coords> coords = new ArrayList<>();
        if(option == 1 || index != 0) {
            for (int i = 0; i < sizeGridX; i++) {
                for (int j = 0; j < sizeGridY; j++) {
                    int endX = listBuild.get(index).getX() + i;
                    int endY = listBuild.get(index).getY() + j;
                    if (endX < sizeGridX && endY < sizeGridY) {
                        if (checkIsFree(i, j, endX, endY)) {
                            coords.add(new Coords(i, j, endX, endY));
                        }
                    }
                }
            }
            if(index == 0) hdvCoords = coords.get(0);
        } else if (option == 2) {
            int sx = sizeGridX/2;
            int sy = sizeGridY/2;
            int ex = listBuild.get(index).getX();
            int ey = listBuild.get(index).getY();
            sx -= (ex/2);
            sy -= (ey/2);
            coords.add(new Coords(sx, sy, ex + sx, ey + sy));
            hdvCoords = coords.get(0);

        }
        if(!coords.isEmpty()) {
            mapCoords.put(listBuild.get(index), coords);
            if(index != 0){//index = 0
                List<Coords> toRemove = new ArrayList<>();
                int[][] copy = new int[sizeGridX][sizeGridY];
                for(Coords coord : coords){
                    copyTab(copy, field);
                    int sx = coord.getStartX();
                    int sy = coord.getStartY();
                    int ex = coord.getEndX();
                    int ey = coord.getEndY();
                    for (int i = sx; i < ex; i++) {
                        for (int j = sy; j < ey; j++) {
                            copy[i][j] = listBuild.get(index).getValue();
                        }
                    }
                    if(!buildRoads(copy)) toRemove.add(coord);
                }
                if(!toRemove.isEmpty()){
                    for(Coords coord : toRemove) coords.remove(coord);
                }
                if(!coords.isEmpty()) mapCoords.replace(listBuild.get(index),coords);
                else mapCoords.remove(listBuild.get(index));
            }
            if(mapCoords.containsKey(listBuild.get(index))) {
                int place;
                if (index == 0) place = 0;
                else place = coords.size() - 1;
                nbBuildPlaced ++;

                int sx = mapCoords.get(listBuild.get(index)).get(place).getStartX();
                int sy = mapCoords.get(listBuild.get(index)).get(place).getStartY();
                int ex = mapCoords.get(listBuild.get(index)).get(place).getEndX();
                int ey = mapCoords.get(listBuild.get(index)).get(place).getEndY();
                for (int i = sx; i < ex; i++) {
                    for (int j = sy; j < ey; j++) {
                        field[i][j] = listBuild.get(index).getValue();
                    }
                }

            }
        }
    }

    public boolean buildRoads(int[][] field){
        Stack<Coords> stack = new Stack<>();
        List<Integer> list = new ArrayList<>();
        for(int y = hdvCoords.getStartY(); y < hdvCoords.getEndY(); y++){
            if(field[hdvCoords.getEndX()][y] == 0) stack.push(new Coords(hdvCoords.getEndX(),y));
        }
        for(int x = hdvCoords.getStartX(); x < hdvCoords.getEndX(); x++){
            if(field[x][hdvCoords.getEndY()] == 0) stack.push(new Coords(x,hdvCoords.getEndY()));
        }
        if(option == 2){
            for(int y = hdvCoords.getEndY(); y >= hdvCoords.getStartY() ; y--){
                if(field[hdvCoords.getStartX() - 1][y] == 0) stack.push(new Coords(hdvCoords.getStartX() - 1,y));
            }
            for(int x = hdvCoords.getEndX(); x >= hdvCoords.getStartX(); x--){
                if(field[x][hdvCoords.getStartY() - 1] == 0) stack.push(new Coords(x,hdvCoords.getStartY() - 1));
            }
        }
        while(!stack.isEmpty()){
            Coords temp = stack.pop();
            field[temp.getStartX()][temp.getStartY()] = ROAD_VALUE;
            if(temp.getStartX() > 0){
                if(field[temp.getStartX()-1][temp.getStartY()] == 0) stack.push(new Coords(temp.getStartX()-1, temp.getStartY()));
                else if(field[temp.getStartX()-1][temp.getStartY()] > 0 &&
                        !list.contains(field[temp.getStartX()-1][temp.getStartY()])) list.add(field[temp.getStartX()-1][temp.getStartY()]);
            }
            if(temp.getStartX() < sizeGridX-1){
                if(field[temp.getStartX()+1][temp.getStartY()] == 0) stack.push(new Coords(temp.getStartX()+1, temp.getStartY()));
                else if(field[temp.getStartX()+1][temp.getStartY()] > 0 &&
                        !list.contains(field[temp.getStartX()+1][temp.getStartY()])) list.add(field[temp.getStartX()+1][temp.getStartY()]);
            }
            if(temp.getStartY() > 0){
                if(field[temp.getStartX()][temp.getStartY()-1] == 0) stack.push(new Coords(temp.getStartX(), temp.getStartY()-1));
                else if(field[temp.getStartX()][temp.getStartY()-1] > 0 &&
                        !list.contains(field[temp.getStartX()][temp.getStartY()-1])) list.add(field[temp.getStartX()][temp.getStartY()-1]);
            }
            if(temp.getStartY() < sizeGridY-1){
                if(field[temp.getStartX()][temp.getStartY()+1] == 0) stack.push(new Coords(temp.getStartX(), temp.getStartY()+1));
                else if(field[temp.getStartX()][temp.getStartY()+1] > 0 &&
                        !list.contains(field[temp.getStartX()][temp.getStartY()+1])) list.add(field[temp.getStartX()][temp.getStartY()+1]);
            }
        }
        return list.size() == mapCoords.keySet().size();
    }

    public int evalField(){
        int cpt = 0;
        for(int i = 0 ; i < sizeGridX; i++) {
            for (int j = 0; j < sizeGridY; j++) {
                if(field[i][j] > 0) cpt++;
            }
        }
        return cpt;
    }

    public void greedyMethod(String option){
        List<Building> subList = listBuild.subList(1, listBuild.size());
        switch (option) {
            case "AireMax" -> {
                System.out.println("AireMax");
                subList.sort((b1, b2) -> (b2.getX() * b2.getY()) - (b1.getX() * b1.getY()));
            }
            case "Encombrement" -> {
                System.out.println("Encombrement");
                subList.sort((b1, b2) -> (b2.getX() + b2.getY()) - (b1.getX() + b1.getY()));
            }
            default -> System.out.println("Default");
        }
        if(option.equals("Random")) {
            for(int i = 0 ; i < 1000; i++) {
                loadFile();
                List<Building> subListRandom = listBuild.subList(1, listBuild.size());
                Collections.shuffle(subListRandom);
                int nbPlace = 0;
                //Placement du hdv
                while (nbPlace < nbBuild) {
                    placeBuild(nbPlace);
                    nbPlace++;
                }

                buildRoads(field);
                nbPlaced.add(nbBuildPlaced);
                nbBuildPlaced = 0;
                nbCasesPlaced.add(evalField());
            }
        } else {
            int nbPlace = 0;
            //Placement du hdv
            while (nbPlace < nbBuild) {
                placeBuild(nbPlace);
                nbPlace++;
            }
            buildRoads(field);
        }
    }

    public List<Integer> getNbPlaced () {
        return nbPlaced;
    }

    public List<Integer> getNbCasesPlaced () {
        return nbCasesPlaced;
    }

    public boolean checkIsFree(int startX, int startY, int endX, int endY){
        boolean test = false;
        for(int i = startX ; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                if(field[i][j] == 0)
                    test = true;
                else if(field[i][j] != 0) {
                    return false;
                }
            }
        }
        return test;
    }

    public int getNbBuildPlaced () {
        return nbBuildPlaced;
    }

    public static final String RESET = "\033[0m";
    public static final String BLACK = "\033[0;30m";   // BLACK
    //Affichage du terrain en mode console
    public void printField() {
        for(int i = 0 ; i < sizeGridX; i++) {
            for (int j = 0; j < sizeGridY; j++) {
                if(field[i][j] == 0 )
                    System.out.printf(RESET + "|%2s" + RESET,field[i][j]);
                else if (field[i][j] == -1)
                    System.out.printf(BLACK + "|%2s"  + RESET, "R");
                else {
                    for (int b = 0; b < nbBuild ; b++) {
                        if (field[i][j] == b +1) {
                            System.out.printf(listBuild.get(b).getColor() + "|%2s" + RESET, field[i][j] );
                            break;
                        }
                    }
                }

            }
            System.out.print("|");
            System.out.println();
        }
        System.out.println();
    }

    //Méthode pour copier le contenu d'un tableau dans un autre (les deux tableaux ayant la même taille)
    public void copyTab(int[][] copy, int[][] copied){
        for(int i = 0; i < sizeGridX; i++){
            for(int j = 0; j < sizeGridY; j++){
                copy[i][j] = copied[i][j];
            }
        }
    }

    //Methode pour pouvoir ecrire sous console la classe complete
    @Override
    public String toString() {
        return "ReadFile :" +
                "field = " + Arrays.toString(field) + "\n" +
                "nbBuild = " + nbBuild + "\n" +
                "sizeGridWidth = " + sizeGridX + "\n" +
                "sizeGridHeight = " + sizeGridY + "\n" +
                "listBuild = " + listBuild + "\n" ;
    }
}