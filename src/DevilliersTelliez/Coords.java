package DevilliersTelliez;

public class Coords {

	private int startX;
	private int startY;
	private int endX;
	private int endY;

	public Coords(int sX, int sY) {
		this.startX = sX;
		this.startY = sY;
		this.endX = 0;
		this.endY = 0;
	}

	public Coords(int sX, int sY, int eX, int eY ) {
		this.startX = sX;
		this.startY = sY;
		this.endX = eX;
		this.endY = eY;
	}

	public int getStartX () {
		return startX;
	}

	public int getStartY () {
		return startY;
	}

	public int getEndX () {
		return endX;
	}

	public int getEndY () {
		return endY;
	}

	@Override
	public String toString() {
		return "\n" + "Coords : " + "\n" +
				"startX = " + startX + "\n" +
				"startY = " + startY + "\n" +
				"endX = " + endX + "\n" +
				"endY = " + endY ;
	}
}
