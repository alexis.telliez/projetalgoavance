# Projet : algorithmes gloutons pour le placement de bâtiments.

Auteurs:
- Alexis Telliez
- Matthias Devilliers

# Questions :
- [x] 1 : L'implémentation des données 
- [x] 2 : Test d'affichage
- [X] 3 : Début algo glouton
- [X] 4 : Gestion conflit bâtiments
- [X] 5 : Association d'un score
- [X] 6 : Variantes
- [X] 7 : Autre variante
- [ ] 8 : Rapport de projet

